﻿#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

const char X = 'X';
const char O = 'O';
const char EMPTY = ' ';
const char TIE = 'T';
const char NO_ONE = 'N';

void instructions();
char askYesNo(string question);
int askNumber(string question, int high, int low = 0);
char humanPiece();
char opponent(char piece);
void displayBoard(const vector<char>& board);
char winner(const vector<char>& board);
bool isLegal(const vector<char>& board, int move);
int humanMove(const vector<char>& board, char human);
int computerMove(const vector<char>& board, char computer);
void announceWinner(char winner, char computer, char human);

void instructions()
{
	cout << "Welcome to Tic - Tac - toe!" << endl;
	cout << "Here you will confront the computer" << endl;
	cout << "Make your move known by entering a number, 1 - 9. The number." << endl;
	cout << "Corresponds to the desired board position, as illustrated:" << endl << endl;
	cout << " 1 | 2 | 3" << endl;
	cout << "----------" << endl;
	cout << " 4 | 5 | 6" << endl;
	cout << "----------" << endl;
	cout << " 7 | 8 | 9" << endl;
	cout << "Prepare and then we start" << endl << endl;
	system("pause");
}

char askYesNo(string question)
{
	char response;
	do
	{
		cout << question << " (y/n): ";
		cin >> response;
	} while (response != 'y' && response != 'n');
	return response;
}

int askNumber(string question, int high, int low)
{
	int number;
	do
	{
		cout << question << " (" << low << " - " << high << "): ";
		cin >> number;
	} while (number > high || number < low);
	return number;
}

char humanPiece()
{
	char go_first = askYesNo("Do you require the first move?");
	if (go_first == 'y')
	{
		return X;
	}
	else
	{
		return O;
	}
}

char opponent(char piece)
{
	if (piece == X)
	{
		return O;
	}
	if (piece == O)
	{
		return X;
	}
}

void displayBoard(const vector<char>& board)
{
	cout << "\n\t" << board[0] << " | " << board[1] << " | " << board[2];
	cout << "\n\t" << "----------";
	cout << "\n\t" << board[3] << " | " << board[4] << " | " << board[5];
	cout << "\n\t" << "----------";
	cout << "\n\t" << board[6] << " | " << board[7] << " | " << board[8];
	cout << endl << endl;
}

char winner(const vector<char>& board)
{
	const int WINNINGROWS[8][3] = { {0,1,2},
		{3,4,5},
		{6,7,8},
		{0,3,6},
		{1,4,7},
		{2,5,8},
		{0,4,8},
		{2,4,6}
	};
	const int TOTAL_ROWS = 8;
	for (int row = 0; row < TOTAL_ROWS; row++)
	{
		if ((board[WINNINGROWS[row][0]] != EMPTY) && 
			(board[WINNINGROWS[row][0]] == board[WINNINGROWS[row][1]])&&
			(board[WINNINGROWS[row][1]] == board[WINNINGROWS[row][2]]))
		{
			return board[WINNINGROWS[row][0]];
		}
	}
	if (count(board.begin(), board.end(), EMPTY) == 0)
	{
		return TIE;
	}
	return NO_ONE;
}

inline bool isLegal(const vector<char>& board, int move)
{
	return (board[move] == EMPTY);
}

int humanMove(const vector<char>& board, char human)
{
	int move = askNumber("Where will you move?", (board.size()), 1);
	while (!isLegal(board, move - 1))
	{
		cout << endl << "That square is already occupied." << endl;
		move = askNumber("Where will you move?", (board.size()), 1);
	}
	return (move - 1);
}

int computerMove(vector<char>& board, char computer)
{
	unsigned short int move = 0;
	bool found = false;
	while (!found && move < board.size())
	{
		if (isLegal(board, move))
		{
			board[move] = computer;
			found = winner(board) == computer;
			board[move] = EMPTY;
		}
		if (!found)
		{
			++move;
		}
	}
	if (!found)
	{
		move = 0;
		char human = opponent(computer);
		while (!found && move < board.size())
		{
			if (isLegal(board, move))
			{
				board[move] = human;
				found = winner(board) == human;
				board[move] = EMPTY;
			}
			if (!found)
			{
				++move;
			}
		}
	}
	if (!found)
	{
		move = 0;
		unsigned int i = 0;
		const int BEST_MOVES[] = { 4, 0, 2, 6, 8, 1, 3, 5, 7 };
		while (!found && i < board.size())
		{
			move = BEST_MOVES[i];
			if (isLegal(board, move))
			{
				found = true;
			}
			++i;
		}
	}
	return move;
}

void announceWinner(char winner, char computer, char human)
{
	if (winner == computer)
	{
		cout << winner << "'s won!" << endl;
		cout << "Good luck next time human!" << endl;
	}
	else if (winner == human)
	{
		cout << winner << "'s won!" << endl;
		cout << "You win, this time." << endl;
	}
	else
	{
		cout << "It's a tie!" << endl;
	}
}

int main()
{
	int move;
	const int NUM_SQUARES = 9;
	vector<char> board(NUM_SQUARES, EMPTY);
	instructions();
	char human = humanPiece();
	char computer = opponent(human);
	char turn = X;
	displayBoard(board);
	while (winner(board) == NO_ONE)
	{
		if (turn == human)
		{
			move = humanMove(board, human);
			board[move] = human;
		}
		else
		{
			move = computerMove(board, computer);
			board[move] = computer;
		}
		displayBoard(board);
		turn = opponent(turn);
	}
	announceWinner(winner(board), computer, human);
	return 0;
}